These functions implement the method described in the Appendix of
[Gandy, Kvaloy, Bottle & Zhou (2010), Biometrika 97 (2): 375-388](https://dx.doi.org/10.1093/biomet/asq004). The implementation is written in R. 

The code has not been extensively tested - so it should be treated
 with caution.  Please let me know of any error/problems that you
 encounter.
 
Source the code

	source("Nevents.R")
		
Computes E(N(\tau)) for given c=3

	getENevents(c=3)
		
Computes P(N(\tau)<=NMax)

	getPNevents(c=3,Nmax=1000)
		
Compute thresold to get a desired in-control performance

	getthresholdENevents(rho=1.25,EN0=1000)	
	getthresholdPNevents(rho=1.25,Nmax=100, PN0=0.01)

You can download all files in a ZIP file [here](https://bitbucket.org/agandy/10_monitortte_calibration/downloads).
